import random as rand
import numpy as np

########################################################
def getBoundingBox(triDat):
############################
  triNums = triDat.shape[0]
  print(triNums)
 

  triXMin = np.inf
  triYMin = np.inf
  triZMin = np.inf
  
  triXMax = -np.inf
  triYMax = -np.inf
  triZMax = -np.inf

  for i in range(0,triNums):
    # Get triXmin
    triXMin = min(triDat[i][0],triDat[i][3],triDat[i][6],triXMin)
    # Get triYmin
    triYMin = min(triDat[i][1],triDat[i][4],triDat[i][7],triYMin)
    # Get triZmin
    triZMin = min(triDat[i][2],triDat[i][5],triDat[i][8],triZMin)
  
    # Get triXmax
    triXMax = max(triDat[i][0],triDat[i][3],triDat[i][6],triXMax)
    # Get triYmax
    triYMax = max(triDat[i][1],triDat[i][4],triDat[i][7],triYMax)
    # Get triZmax
    triZMax = max(triDat[i][2],triDat[i][5],triDat[i][8],triZMax)
  
  return triXMin, triYMin, triZMin, triXMax, triYMax, triZMax

############################

# -19 to 21
#for i in range (0,10):
#  x=20*(rand.random()-0.5)+1
#  print(x)

tri = np.loadtxt('../testInputs/dragon_vrip.dat',skiprows=1)
boundingBox = getBoundingBox(tri)
print(boundingBox)

filO = open('short_new_bonds100000.dat','w')

filO.write( 'Bonds 100000\n')
for i in range(0,100000):
  x0 = (boundingBox[3]-boundingBox[0])*(rand.random()-0.5) \
           +(boundingBox[3]+boundingBox[0])/2.
  y0 = (boundingBox[4]-boundingBox[1])*(rand.random()-0.5) \
           +(boundingBox[4]+boundingBox[1])/2.
  z0 = (boundingBox[5]-boundingBox[2])*(rand.random()-0.5) \
           +(boundingBox[5]+boundingBox[2])/2.

  x1 = 0.1*(boundingBox[3]-boundingBox[0])*(rand.random()-0.5)+x0

  y1 = 0.1*(boundingBox[4]-boundingBox[1])*(rand.random()-0.5)+y0

  z1 = 0.1*(boundingBox[5]-boundingBox[2])*(rand.random()-0.5)+z0

  filO.write('{} {} {} {} {} {}\n'.format(x0,y0,z0,x1,y1,z1))

filO.close()
