from __future__ import print_function
import numpy as np
import re

def importMesh(fname,oname):

  testLst = [1,2,3,4,5,6]

  v = []
  f = []
  cnt = 0

  with open(fname) as fil:
    for line in fil:

      cnt += 1

      line = line.strip()

    # For testing, print line

      #print(line)

    # Get vertices and faces

      vBool = re.search('\Av ',line)
      if (vBool):
        #print('    vertex line: {}'.format(line))
	v.append(line.strip('vn'))

      fBool = re.search('\Af',line)
      if (fBool):
        #print('    face line: {}'.format(line))
        toF = re.findall('\d+',line)
	#print(toF)
	del toF[5]
	del toF[3]
	del toF[1]
	#print(toF)
	f.append(toF)

  #print(v)
  #print(f)
  #print(f[0])
  #print(f[0][0])
  #print(testLst[int(f[0][0])])

  #print('number of vertices: {}'.format(len(v)))

  filO = open(oname,'w')

  for face in f:

    #print(face)

    for vert in face:

      #print(vert)

      filO.write(v[int(vert)-1])

    filO.write('\n')

  filO.close()

  oPut = np.loadtxt(oname)
  print(oPut.shape)

  return 0
