# Set C++ compiler.
#cpp = g++
cpp = nvcc

# Set C++ flag.
cppFlags = -std=c++11 -arch=sm_30 -I cub 
#cppFlags = -D_FORCE_INLINES -gencode -arch=sm_30 -std=c++11 -D DEBUG -D PIN -I cub $(oFlags)
oFlags = -O3
dFlags = -G -Xcompiler -rdynamic -lineinfo
dFlags2 = -g -Wall -Wextra

# Set NVCC flags.
NVCC = nvcc
NVCCFLAGS = -std=c++11 -lineinfo --ptxas-options=-v -O \
        -gencode arch=compute_35,code=sm_35 \
        -gencode arch=compute_30,code=compute_30 \
        -gencode arch=compute_60,code=compute_60 \
        -gencode arch=compute_70,code=compute_70

LFLAGS = -lcudart_static -lrt

#make : src/calcIntersections.cpp
make : src/quadratic.cu
	$(NVCC) $(NVCCFLAGS) -o quadratic src/quadratic.cu $(LFLAGS)


#debug : src/calcIntersections.cpp
debug : src/quadratic.cu
	g++ -std=c++0x -g -o calcIntersectionsDebug src/calcIntersections.cpp
