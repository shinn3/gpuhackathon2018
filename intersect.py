import numpy as np
import readObj as obj
import time

########################################################
def calcIntersect(triDat,linDat):
############################

  if (not checkBoxOverlap(triDat,linDat)):
    return 'no intersection. no box overlap'

  vec12X = triDat[3]-triDat[0]
  vec12Y = triDat[4]-triDat[1]
  vec12Z = triDat[5]-triDat[2]

  vec13X = triDat[6]-triDat[0]
  vec13Y = triDat[7]-triDat[1]
  vec13Z = triDat[8]-triDat[2]

  vec12 = np.array([vec12X, vec12Y, vec12Z])
  vec13 = np.array([vec13X, vec13Y, vec13Z])

  triNorm = np.cross(vec12,vec13)

  linDirX = linDat[3]-linDat[0]
  linDirY = linDat[4]-linDat[1]
  linDirZ = linDat[5]-linDat[2]

  linVec = np.array([linDirX, linDirY, linDirZ])

  ndotu = np.dot(linVec,triNorm)
  if abs(ndotu) < epsilon:
    return 'no intersection'
    

  wX = linDat[0]-triDat[0]
  wY = linDat[1]-triDat[1]
  wZ = linDat[2]-triDat[2]
  w = np.array([wX,wY,wZ])

  si = -np.dot(triNorm,w) / ndotu

  if (-1<=si and si<=1):
    planePoint = np.array([triDat[0],triDat[1],triDat[2]])

    psi = w + si * linVec + planePoint
    return psi
  else:
    return 'no intersection. segment too short'

########################################################
def checkBoxOverlap(triDat,linDat):
############################

  # Get triXmin
  triXMin = min(triDat[0],triDat[3],triDat[6])
  # Get triYmin
  triYMin = min(triDat[1],triDat[4],triDat[7])
  # Get triZmin
  triZMin = min(triDat[2],triDat[5],triDat[8])

  # Get triXmin
  triXMax = max(triDat[0],triDat[3],triDat[6])
  # Get triYmin
  triYMax = max(triDat[1],triDat[4],triDat[7])
  # Get triZmin
  triZMax = max(triDat[2],triDat[5],triDat[8])

  # Get linXmin
  linXMin = min(linDat[0],linDat[3])
  # Get linYmin
  linYMin = min(linDat[1],linDat[4])
  # Get linZmin
  linZMin = min(linDat[2],linDat[5])

  # Get linXmin
  linXMax = max(linDat[0],linDat[3])
  # Get linYmin
  linYMax = max(linDat[1],linDat[4])
  # Get linZmin
  linZMax = max(linDat[2],linDat[5])

  if (triXMax >= linXMin and linXMax >= triXMin and \
      triYMax >= linYMin and linYMax >= triYMin and \
      triZMax >= linZMin and linZMax >= triZMin):

    print('box overlap')
    print('{{{{{} {} {}}} {{{} {} {}}}}}'.format(triXMin,triYMin,triZMin,triXMax,triYMax,triZMax))
    print('{{{{{} {} {}}} {{{} {} {}}}}}'.format(linXMin,linYMin,linZMin,linXMax,linYMax,linZMax))
    return 1

  else:

    return 0

############################

start = time.time()

tri = np.loadtxt('testTriangles.dat')
lin = np.loadtxt('testBond.dat')
print(time.time()-start)
print(tri.shape)
print(lin.shape)

epsilon = 1e-6

for i in range(0,2):
	print('triangle')
	for j in range (0,12):		
		print(calcIntersect(tri[i],lin[j]))

print(time.time()-start)
