#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class Timer
{
public:
    Timer() : beg_(clock_::now()) {}
    void reset()
    {
        beg_ = clock_::now();
    }
    double elapsed() const
    { 
        return std::chrono::duration_cast<second_>
            (clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};
struct Vertex {
    float x, y, z;
};
struct Bond {
    Vertex v1, v2;
};
struct Triangle {
    Vertex v1, v2, v3;
};

unsigned getSizeT (ifstream & file)
{
    string headerT;
    unsigned sizeT;
    file >> headerT;
    file >> sizeT;
    cout << "headerT: " << headerT << " sizeT: " << sizeT << endl;
    return sizeT;
}

Triangle* readTri (ifstream & file, unsigned sizeT)
{
    int i;
    Triangle* a = NULL;
    a = new Triangle[sizeT];

    for (i=0; i<sizeT; i++)
    {
        file >> a[i].v1.x; file >> a[i].v1.y; file >> a[i].v1.z;
        file >> a[i].v2.x; file >> a[i].v2.y; file >> a[i].v2.z;
        file >> a[i].v3.x; file >> a[i].v3.y; file >> a[i].v3.z;
    }
    return a;
}

void sceneBox (Triangle* a, unsigned sizeT, Vertex & bboxMin, Vertex & bboxMax)
{// Determine min and max X, Y, and Z in set of triangles.
 // These determine the scene bounding box.
    for (int i=0; i<sizeT; ++i)
    {
        if (i==0)
        {
            bboxMin.x = min(a[i].v1.x, min(a[i].v2.x,a[i].v3.x));
            bboxMin.y = min(a[i].v1.y, min(a[i].v2.y,a[i].v3.y));
            bboxMin.z = min(a[i].v1.z, min(a[i].v2.z,a[i].v3.z));
        
            bboxMax.x = max(a[i].v1.x, max(a[i].v2.x,a[i].v3.x));
            bboxMax.y = max(a[i].v1.y, max(a[i].v2.y,a[i].v3.y));
            bboxMax.z = max(a[i].v1.z, max(a[i].v2.z,a[i].v3.z));
        }
        bboxMin.x = min(bboxMin.x, min(a[i].v1.x, min(a[i].v2.x,a[i].v3.x)));
        bboxMin.y = min(bboxMin.y, min(a[i].v1.y, min(a[i].v2.y,a[i].v3.y)));
        bboxMin.z = min(bboxMin.z, min(a[i].v1.z, min(a[i].v2.z,a[i].v3.z)));
        
        bboxMax.x = max(bboxMax.x, max(a[i].v1.x, max(a[i].v2.x,a[i].v3.x)));
        bboxMax.y = max(bboxMax.y, max(a[i].v1.y, max(a[i].v2.y,a[i].v3.y)));
        bboxMax.z = max(bboxMax.z, max(a[i].v1.z, max(a[i].v2.z,a[i].v3.z)));
    }
    cout << "Bounding box: \n Min: ";
    cout << bboxMin.x << " " << bboxMin.y << " " << bboxMin.z << endl;
    cout << " Max: ";
    cout << bboxMax.x << " " << bboxMax.y << " " << bboxMax.z << endl;
}

void calcTriBBox (Triangle* a,
                  Vertex* tri_bboxMin,
                  Vertex* tri_bboxMax,
                  unsigned sizeT)
{
    for (int i=0; i<sizeT; ++i)
    {
        tri_bboxMin[i].x = min(a[i].v1.x, min(a[i].v2.x,a[i].v3.x));
        tri_bboxMin[i].y = min(a[i].v1.y, min(a[i].v2.y,a[i].v3.y));
        tri_bboxMin[i].z = min(a[i].v1.z, min(a[i].v2.z,a[i].v3.z));
        
        tri_bboxMax[i].x = max(a[i].v1.x, max(a[i].v2.x,a[i].v3.x));
        tri_bboxMax[i].y = max(a[i].v1.y, max(a[i].v2.y,a[i].v3.y));
        tri_bboxMax[i].z = max(a[i].v1.z, max(a[i].v2.z,a[i].v3.z));
    }
}

bool checkBoxOverlap
        (Vertex * tri_bboxMin, Vertex * tri_bboxMax, Vertex * grid_bboxMin,
         float NLX, float NLY, float NLZ, int rx, int ry, int rz)
{
    if(    tri_bboxMin->x <= (rx+1) * NLX + grid_bboxMin->x
        && tri_bboxMax->x >=  rx    * NLX + grid_bboxMin->x
        && tri_bboxMin->y <= (ry+1) * NLY + grid_bboxMin->y
        && tri_bboxMax->y >=  ry    * NLY + grid_bboxMin->y
        && tri_bboxMin->z <= (rz+1) * NLZ + grid_bboxMin->z
        && tri_bboxMax->z >=  rz    * NLZ + grid_bboxMin->z)
    { return 1; }
    else
    { return 0; }
}
void calcTriCentroid()
{
}

int main ()
{
    float inv3 = 1/3.;
    float epsilon = 0.000001;
    cout << inv3 << endl;
    Timer tmr;
    double stopwatch = tmr.elapsed();
    cout << stopwatch << endl;

    double subsA0, subsA1, subsA2, subsB0, subsB1, subsB2;

    stopwatch = tmr.elapsed();
    cout << stopwatch << endl;

    string userInput;
    cout << "Input triangle file name (testInputs/dragon_vrip.dat): ";
    getline(cin,userInput);

    ifstream inputFileT;
    inputFileT.open(userInput);

    int i=0,j=0;

    // Assign triangle vertex coordinates to matrix 
    //
    // Array containing vertices of each triangle face
    //

    string headerT;
    unsigned sizeT = getSizeT(inputFileT);

    // Data containing values of cross product
    double* crossx = NULL;
    double* crossy = NULL;
    double* crossz = NULL;

    crossx= new double[sizeT];
    crossy= new double[sizeT];
    crossz= new double[sizeT];


    Triangle* a = NULL;
    a = readTri(inputFileT, sizeT);
    tmr.reset();
    
    stopwatch = tmr.elapsed();
    cout << "Triangle read time: " << stopwatch << endl;
    inputFileT.close();
   
    // Generate grid cells
    cout << "Determining scene bounding box..." << endl;
    Vertex bboxMin, bboxMax;
    sceneBox(a, sizeT, bboxMin, bboxMax);
    
    cout << "Subdividing bounding box into grid cells..." << endl;
    Vertex bboxLen; //Box side lengths
    bboxLen.x = bboxMax.x - bboxMin.x + epsilon;
    bboxLen.y = bboxMax.y - bboxMin.y + epsilon;
    bboxLen.z = bboxMax.z - bboxMin.z + epsilon;

    cout << "Bounding box dimensions: ";
    cout << bboxLen.x << " " << bboxLen.y << " " << bboxLen.z << endl;

    int bboxNX, bboxNY, bboxNZ;// Number of cells in each dimension
    float bboxNX_inv, bboxNY_inv, bboxNZ_inv;// Inverse number of cells
    float bboxNLX, bboxNLY, bboxNLZ;// cell side lengths

    float cbRoot = cbrt(4*sizeT/(bboxLen.x*bboxLen.y*bboxLen.z));
    cout << "cbRoot: " << cbRoot << endl;

    bboxNX = bboxLen.x*cbRoot+1;
    bboxNY = bboxLen.y*cbRoot+1;
    bboxNZ = bboxLen.z*cbRoot+1;

    bboxNX_inv = 1/float(bboxNX);
    bboxNY_inv = 1/float(bboxNY);
    bboxNZ_inv = 1/float(bboxNZ);

    bboxNLX = bboxLen.x*bboxNX_inv;
    bboxNLY = bboxLen.y*bboxNY_inv;
    bboxNLZ = bboxLen.z*bboxNZ_inv;

    cout << "bboxNLX * bboxNX = ";
    cout << bboxNLX << " * " << bboxNX << " = " << bboxLen.x << endl;
    cout << "bboxNLY * bboxNY = ";
    cout << bboxNLY << " * " << bboxNY << " = " << bboxLen.y << endl;
    cout << "bboxNLZ * bboxNZ = ";
    cout << bboxNLZ << " * " << bboxNZ << " = " << bboxLen.z << endl;
    
    cout << "Bounding box resolution (# of boxes in each dimension): " << endl;
    cout << bboxNX << " " << bboxNY << " " << bboxNZ << endl;

    int totN = bboxNX*bboxNY*bboxNZ;

    // Calculate triangle bounding boxes
    Vertex* tri_bboxMin = NULL;
    Vertex* tri_bboxMax = NULL;
    tri_bboxMin = new Vertex[sizeT];
    tri_bboxMax = new Vertex[sizeT];
   
    calcTriBBox(a, tri_bboxMin, tri_bboxMax, sizeT);

    // Assign triangles to grid cell
    int triCnt;

    int** cellTriId
        = (int**) malloc(sizeof(int*)*totN); // Contains indices of triangles
    memset(cellTriId,0,sizeof(int*)*totN);

    int* cellTriNum = NULL; // Contains number of triangles in each cell
    cellTriNum = new int[ bboxNX * bboxNY * bboxNZ ];
    cout << "Total number of cells: ";
    cout <<  bboxNX * bboxNY * bboxNZ  << endl;

    int id;

    for(int rz=0; rz<bboxNZ; ++rz)
    {
        for(int ry=0; ry<bboxNY; ++ry)
        {
            for(int rx=0; rx<bboxNX; ++rx)
            {
                triCnt = 0;
                for(i=0; i<sizeT; ++i)
                {
                    if(checkBoxOverlap
                          (&tri_bboxMin[i], &tri_bboxMax[i], &bboxMin,
                           bboxNLX, bboxNLY, bboxNLZ, rx, ry, rz))
                    {
                        cout << "yes\n";
                        ++triCnt;
                    }
                }
                cellTriId[rx+bboxNX*ry+bboxNX*bboxNY*rz] = new int[triCnt];
                cellTriNum[rx+bboxNX*ry+bboxNX*bboxNY*rz] = triCnt;
                cout << "Number of triangles in cell";
                cout << rx+bboxNX*ry+bboxNX*bboxNY*rz << ": " << triCnt << endl;

                id = 0;

                for(i=0; i<sizeT; ++i)
                {
                    triCnt = 0;
                    if(checkBoxOverlap
                          (&tri_bboxMin[i], &tri_bboxMax[i], &bboxMin,
                           bboxNLX, bboxNLY, bboxNLZ, rx, ry, rz))
                    {
                        cellTriId[rx+bboxNX*ry+bboxNX*bboxNY*rz][id] = i;
                        cout << id << ":" << i << " " ;
                        id++;
                    }
                }
               cout << endl;
            }
           // cout << ry << " ";
        }
        //cout << rz << endl;
    }

    // Get point on plane (centroid)
    // For now there are only 2 triangles

    Vertex* cent = NULL;

    cent = new Vertex[sizeT];

    for (i=0; i<sizeT; i++)
    {
        //cout << "i: " << i << endl;

        cent[i].x = (a[i].v1.x+a[i].v2.x+a[i].v3.x)*inv3;
        cent[i].y = (a[i].v1.y+a[i].v2.y+a[i].v3.y)*inv3;
        cent[i].z = (a[i].v1.z+a[i].v2.z+a[i].v3.z)*inv3;

        //cout << setw(10) << "Centroid: ";
        //cout << setw(13) << cent[i].x;
        //cout << setw(13) << cent[i].y;
        //cout << setw(13) << cent[i].z << endl;
	
    // Substract two matrices
        subsA0 = a[i].v2.x-a[i].v1.x;
        subsA1 = a[i].v2.y-a[i].v1.y;
        subsA2 = a[i].v2.z-a[i].v1.z;

        subsB0 = a[i].v3.x-a[i].v2.x;
        subsB1 = a[i].v3.y-a[i].v2.y;
        subsB2 = a[i].v3.z-a[i].v2.z;

        //cout << setw(10) << "A: ";
        //cout << setw(13) << subsA0;
        //cout << setw(13) << subsA1;
        //cout << setw(13) << subsA2 << endl;

        //cout << setw(10) << "B: ";
        //cout << setw(13) << subsB0;
        //cout << setw(13) << subsB1;
        //cout << setw(13) << subsB2 << endl;
	
    // Calculate cross product 
        crossx[i] = subsA1*subsB2 - subsB1*subsA2;
        crossy[i] = subsB0*subsA2 - subsA0*subsB2;
        crossz[i] = subsA0*subsB1 - subsB0*subsA1;

        //cout << setw(10) << "Cross: ";
        //cout << setw(13) << crossx[i];
        //cout << setw(13) << crossy[i];
        //cout << setw(13) << crossz[i] << endl;
    }
    stopwatch = tmr.elapsed();
    cout << "Triangle calculations: " << stopwatch << endl;

    // Read bonds	
    cout << "Input bond file name (testInputs/full_bonds.dat): ";
    getline(cin,userInput);

    ifstream inputFileBond;
    inputFileBond.open(userInput);
    string headerB;
    unsigned sizeB;

    inputFileBond >> headerB;
    inputFileBond >> sizeB;
    //cout << headerB << " " << sizeB;

    // Bond calculations data
    Vertex* dir = NULL;
    dir = new Vertex[sizeB];

    // Array containing points in each line segment
    Bond* b = NULL;
    b = new Bond[sizeB];

    // Read Bonds
    i = 0;
    j = 0;
    for (i=0; i<sizeB; i++)
    {
        inputFileBond >> b[i].v1.x;
        inputFileBond >> b[i].v1.y;
        inputFileBond >> b[i].v1.z;
        inputFileBond >> b[i].v2.x;
        inputFileBond >> b[i].v2.y;
        inputFileBond >> b[i].v2.z;
    }

    inputFileBond.close();
    tmr.reset();
    
    // Calculate Bond Direction 
    for (i=0; i<sizeB; i++)
    { 
        dir[i].x = b[i].v2.x-b[i].v1.x;
        dir[i].y = b[i].v2.y-b[i].v1.y;
        dir[i].z = b[i].v2.z-b[i].v1.z;

        //cout << setw(10) << "Bond Dir: ";
        //cout << setw(13) << dir[i].x;
        //cout << setw(13) << dir[i].y;
        //cout << setw(13) << dir[i].z << endl;
    }
    
    // Calculate bond bounding boxes
    Vertex* bond_bboxMin = NULL;
    Vertex* bond_bboxMax = NULL;

    bond_bboxMin = new Vertex[sizeB];
    bond_bboxMax = new Vertex[sizeB];

    for (i=0; i<sizeB; ++i)
    {
        bond_bboxMin[i].x = min(b[i].v1.x,b[i].v2.x);
        bond_bboxMin[i].y = min(b[i].v1.y,b[i].v2.y);
        bond_bboxMin[i].z = min(b[i].v1.z,b[i].v2.z);
        
        bond_bboxMax[i].x = max(b[i].v1.x,b[i].v2.x);
        bond_bboxMax[i].y = max(b[i].v1.y,b[i].v2.y);
        bond_bboxMax[i].z = max(b[i].v1.z,b[i].v2.z);
    }
    
    // Determine cells occupied by each bond
    int** bondCellId = (int**) malloc(sizeof(int*)*sizeB);
    memset(bondCellId,0,sizeof(int*)*sizeB);

    int cellCnt;
    double tX, tY, tZ;
    double intersectX, intersectY, intersectZ;
    int k = 0;

    ofstream file;
    file.open("Intersection_pts.pdb");

    // For each bond
    for (i=0; i<sizeB; ++i)
    {
        cellCnt = 0;
        // For each cell
        for (int rz=0; rz<bboxNZ; ++rz)
        {
            for (int ry=0; ry<bboxNY; ++ry)
            {
                for (int rx=0; rx<bboxNX; ++rx)
                {
                    if(bond_bboxMin[i].x<(rx+1)*bboxLen.x*bboxNX_inv+bboxMin.x
                    && bond_bboxMax[i].x> rx   *bboxLen.x*bboxNX_inv+bboxMin.x
                    && bond_bboxMin[i].y<(ry+1)*bboxLen.y*bboxNY_inv+bboxMin.y
                    && bond_bboxMax[i].y> ry   *bboxLen.y*bboxNY_inv+bboxMin.y
                    && bond_bboxMin[i].z<(rz+1)*bboxLen.z*bboxNZ_inv+bboxMin.z
                    && bond_bboxMax[i].z> rz   *bboxLen.z*bboxNZ_inv+bboxMin.z)
                    {// For each triangle in cell, calculate intersection
                        cout << "Bond intersects with cell: " << rx+bboxNX*ry+bboxNY*rz << endl;
                        for (int t0=0; t0<cellTriNum[rx+bboxNX*ry+bboxNY*rz]; ++t0)
                        {// Calculate intersection with triangle
                         // cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]
                            cout << cellTriId[rx+bboxNX*ry+bboxNY*rz][t0] << " ";
///////////////////////////////////////////////////////////////////////////////////
	    k++;
            //cout << " " << dot [i][j] << "\n" ;	
          /*tX = (((cent[i].x) - (b[j].v1.x))*(crossx[i]));
            tY = (((cent[i].y) - (b[j].v1.y))*(crossy[i]));
            tZ = (((cent[i].z) - (b[j].v1.z))*(crossz[i]));*/
            tX = (cent[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]].x
                  - b[j].v1.x)*crossx[i];
            tY = (cent[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]].y
                  - b[j].v1.y) * crossy[i];
            tZ = (cent[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]].z
                  - b[j].v1.z) * crossz[i];

            if (tZ+tY+tZ>=0 && tX+tY+tZ<=1)
            {
                intersectX = dir[j].x * ((tX+tY+tZ) /
                    (dir[j].x * crossx[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].y * crossy[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].z * crossz[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]))
                     + b[j].v1.x;

                intersectY = dir[j].y * ((tX+tY+tZ) /
                    (dir[j].x * crossx[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].y * crossy[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].z * crossz[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]))
                     + b[j].v1.y;

                intersectZ = dir[j].z * ((tX+tY+tZ) /
                    (dir[j].x * crossx[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].y * crossy[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]
                   + dir[j].z * crossz[cellTriId[rx+bboxNX*ry+bboxNY*rz][t0]]))
                     + b[j].v1.z;
            }
            file << "ATOM  ";           // "ATOM"
            file << setw(5) << i << " ";// Atom number | Bond index
            file << "CA  ";             // Atom name
            file << " ";                // Alternate location indicator
	    file << "HIS ";             // Residue name
            file << "A";                // Chain
            //file << setw(4) << m;       // Residue number | Triangle index
            file << setw(4) << cellTriId[rx+bboxNX*ry+bboxNY*rz][t0];
            file << " " << "   ";       // Code for insertions of residues
            file << setw(8) << fixed << setprecision(5) << intersectX;   // X position
            file << setw(8) << fixed << setprecision(5) << intersectY;   // Y position
            file << setw(8) << fixed << setprecision(5) << intersectZ;   // Z position
	    file << "    0";            // Occupancy
	    file << "                    "; // Everything else
	    file << endl;
///////////////////////////////////////////////////////////////////////////////////
                        }
                        cout << endl;
                        ++cellCnt;
                    }
                }
            }
        }
        bondCellId[i] = new int[cellCnt+1];
        bondCellId[i][0] = cellCnt;
        cellCnt = 1;
        for (int rz=0; rz<bboxNZ; ++rz)
        {
            for (int ry=0; ry<bboxNY; ++ry)
            {
                for (int rx=0; rx<bboxNX; ++rx)
                {
                    if(bond_bboxMin[i].x<(rx+1)*bboxLen.x*bboxNX_inv+bboxMin.x
                    && bond_bboxMax[i].x> rx   *bboxLen.x*bboxNX_inv+bboxMin.x
                    && bond_bboxMin[i].y<(ry+1)*bboxLen.y*bboxNY_inv+bboxMin.y
                    && bond_bboxMax[i].y> ry   *bboxLen.y*bboxNY_inv+bboxMin.y
                    && bond_bboxMin[i].z<(rz+1)*bboxLen.z*bboxNZ_inv+bboxMin.z
                    && bond_bboxMax[i].z> rz   *bboxLen.z*bboxNZ_inv+bboxMin.z)
                    {
                        bondCellId[i][cellCnt] = rx+bboxNX*ry+bboxNY*rz;
                        cout << i << ":" << bondCellId[i][cellCnt] << " ";
                        ++cellCnt;
                    }
                }
            }
        }
        cout << endl;
    }

    /*double* dotx = NULL;
    double* doty = NULL;
    double* dotz = NULL;

    dotx = new double[sizeB];
    doty = new double[sizeB];
    dotz = new double[sizeB];*/
	
//    ofstream file;
//    file.open("Intersection_pts.pdb");
    int m = 0;

    
    tmr.reset();

    /*
    // For each triangle and each bond, calculate intersection
    for (i=0; i<sizeT; i++)
    {
	m++;
        for (j=0;j<sizeB;j++)
        {
            if (k==0) // Timer
            {
            }
	    k++;
            //cout << " " << dot [i][j] << "\n" ;	
            tX = (((centx[i]) - (b[j].v1.x))*(crossx[i]));
            tY = (((centy[i]) - (b[j].v1.y))*(crossy[i]));
            tZ = (((centz[i]) - (b[j].v1.z))*(crossz[i]));

            if (tZ+tY+tZ>=0 && tX+tY+tZ<=1)
            {
                intersectX
                    = dir[j].x * ((tX+tY+tZ)/(  dir[j].x * crossx[i]
                                              + dir[j].y * crossy[i]
                                              + dir[j].z * crossz[i]))
                     +b[j].v1.x;

                intersectY
                    = dir[j].y * ((tX+tY+tZ)/(  dir[j].x * crossx[i]
                                              + dir[j].y * crossy[i]
                                              + dir[j].z * crossz[i]))
                     +b[j].v1.y;

                intersectZ
                    = dir[j].z * ((tX+tY+tZ)/(  dir[j].x * crossx[i]
                                              + dir[j].y * crossy[i]
                                              + dir[j].z * crossz[i]))
                     +b[j].v1.z;
            }

            if (k==1) // Timer
            {
                stopwatch = tmr.elapsed();
                cout << "First intersection calculated: " << stopwatch << endl;
            }

            //x[i] = b[i][3]+t[i]*dir[i][j];
            //cout << "Value of t = " << " " << (tx[i]+ty[i]+tz[i]) << "\n" ;
            //file << "Intersection points are  = " << " " << t1[i] << " "
            //    << t2[i] << " " << t3[i] << "\n" ;
            file << "ATOM  ";           // "ATOM"
            file << setw(5) << k << " ";// Atom number
            file << "CA  ";             // Atom name
            file << " ";                // Alternate location indicator
	    file << "HIS ";             // Residue name
            file << "A";                // Chain
            file << setw(4) << m;       // Residue number
            file << " " << "   ";       // Code for insertions of residues
            file << setw(8) << fixed << setprecision(5) << intersectX;   // X position
            file << setw(8) << fixed << setprecision(5) << intersectY;   // Y position
            file << setw(8) << fixed << setprecision(5) << intersectZ;   // Z position
	    file << "    0";            // Occupancy
	    file << "                    "; // Everything else
	    file << endl;
            
            if (k==1) // Timer
            {
                stopwatch = tmr.elapsed();
                cout << "Intersection calcs and writing: " << stopwatch << endl;
            }
        }
    }
    */
    stopwatch = tmr.elapsed();
    cout << "All intersection calculation time: " << stopwatch << endl;
    file.close();

    return 0;
}
