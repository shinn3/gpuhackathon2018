#include <chrono>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

class Timer
{
public:
    Timer() : beg_(clock_::now()) {}
    void reset()
    {
        beg_ = clock_::now();
    }
    double elapsed() const
    { 
        return std::chrono::duration_cast<second_>
            (clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};
struct Vertex {
    float x, y, z;
};
struct Bond {
    Vertex v1, v2;
};
struct Triangle {
    Vertex v1, v2, v3;
};

Vertex calcIntersect(Triangle * a, Bond * b)
{
    float inv3 = 1/3.0;

    float centX = (a->v1.x+a->v2.x+a->v3.x)*inv3;
    float centY = (a->v1.y+a->v2.y+a->v3.y)*inv3;
    float centZ = (a->v1.z+a->v2.z+a->v3.z)*inv3;

    // Substract two matrices
    float subsA0 = a->v2.x-a->v1.x;
    float subsA1 = a->v2.y-a->v1.y;
    float subsA2 = a->v2.z-a->v1.z;

    float subsB0 = a->v3.x-a->v2.x;
    float subsB1 = a->v3.y-a->v2.y;
    float subsB2 = a->v3.z-a->v2.z;

    // Calculate cross product 
    float crossX = subsA1*subsB2 - subsB1*subsA2;
    float crossY = subsB0*subsA2 - subsA0*subsB2;
    float crossZ = subsA0*subsB1 - subsB0*subsA1;

    double tX = (centX - b->v1.x)*crossX;
    double tY = (centY - b->v1.y)*crossY;
    double tZ = (centZ - b->v1.z)*crossZ;

    // Calculate bond direction
    float dirX = b->v2.x-b->v1.x;
    float dirY = b->v2.y-b->v1.y;
    float dirZ = b->v2.z-b->v1.z;

    float num = tX+tY+tZ;

    float factor = num/( dirX*crossX + dirY*crossY + dirZ*crossZ );

    Vertex intersect;
    if (tZ+tY+tZ>=0 && tX+tY+tZ<=1)
    {
        intersect.x
            = dirX * (tX+tY+tZ)/(  dirX * crossX
                                 + dirY * crossY
                                 + dirZ * crossZ)
             +b->v1.x;

        intersect.y
            = dirY * (tX+tY+tZ)/(  dirX * crossX
                                 + dirY * crossY
                                 + dirZ * crossZ)
             +b->v1.y;

        intersect.z
            = dirZ * (tX+tY+tZ)/(  dirX * crossX
                                 + dirY * crossY
                                 + dirZ * crossZ)
             +b->v1.z;
    }
    return intersect;
}

int main ()
{
    Timer tmr;
    double stopwatch = tmr.elapsed();
    cout << stopwatch << endl;

    double subsA0, subsA1, subsA2, subsB0, subsB1, subsB2;

    stopwatch = tmr.elapsed();
    cout << stopwatch << endl;

    string userInput;
    cout << "Input triangle file name (testInputs/dragon_vrip.dat): ";
    getline(cin,userInput);

    ifstream inputFileT;
    inputFileT.open(userInput);

    int i=0,j=0;

    // Assign triangle vertex coordinates to matrix 
    //
    // Array containing vertices of each triangle face
    //

    string headerT;
    unsigned sizeT;

    inputFileT >> headerT;
    inputFileT >> sizeT;
    
    // Data containing values of cross product
    double* crossx = NULL;
    double* crossy = NULL;
    double* crossz = NULL;

    crossx= new double[sizeT];
    crossy= new double[sizeT];
    crossz= new double[sizeT];

    double* centx = NULL;
    double* centy = NULL;
    double* centz = NULL;

    centx= new double[sizeT];
    centy= new double[sizeT];
    centz= new double[sizeT];

    Triangle* a = NULL;
    a = new Triangle[sizeT];
    //cout << headerT << " " << sizeT;

    tmr.reset();

    for (i=0; i<sizeT; i++)
    {
        inputFileT >> a[i].v1.x;
        inputFileT >> a[i].v1.y;
        inputFileT >> a[i].v1.z;
        inputFileT >> a[i].v2.x;
        inputFileT >> a[i].v2.y;
        inputFileT >> a[i].v2.z;
        inputFileT >> a[i].v3.x;
        inputFileT >> a[i].v3.y;
        inputFileT >> a[i].v3.z;
        //cout<<"\n";
    }
    stopwatch = tmr.elapsed();
    cout << "Triangle read time: " << stopwatch << endl;
    inputFileT.close();
    
    cout << "Input bond file name (testInputs/full_bonds.dat): ";
    getline(cin,userInput);

    ifstream inputFileBond;
    inputFileBond.open(userInput);
    string headerB;
    unsigned sizeB;

    inputFileBond >> headerB;
    inputFileBond >> sizeB;
    //cout << headerB << " " << sizeB;

    // Bond calculations data
    Vertex* dir = NULL;
    dir = new Vertex[sizeB];

    double* dotx = NULL;
    double* doty = NULL;
    double* dotz = NULL;

    dotx = new double[sizeB];
    doty = new double[sizeB];
    dotz = new double[sizeB];

    double dot[12][12] ={0};

    // Array containing points in each line segment
    Bond* b = NULL;
    b = new Bond[sizeB];

    // Read Bonds
    i = 0;
    j = 0;
    cout << "I am outside triangle" << "\n";
    for (i=0; i<sizeB; i++)
    {
        inputFileBond >> b[i].v1.x;
        inputFileBond >> b[i].v1.y;
        inputFileBond >> b[i].v1.z;
        inputFileBond >> b[i].v2.x;
        inputFileBond >> b[i].v2.y;
        inputFileBond >> b[i].v2.z;
    }

    inputFileBond.close();

    tmr.reset();

    // Get point on plane (centroid)
    // For now there are only 2 triangles
    for (i=0; i<sizeT; i++)
    {
        //cout << "i: " << i << endl;

        centx[i] = (a[i].v1.x+a[i].v2.x+a[i].v3.x)/3.0;
        centy[i] = (a[i].v1.y+a[i].v2.y+a[i].v3.y)/3.0;
        centz[i] = (a[i].v1.z+a[i].v2.z+a[i].v3.z)/3.0;

        //cout << setw(10) << "Centroid: ";
        //cout << setw(13) << centx[i];
        //cout << setw(13) << centy[i];
        //cout << setw(13) << centz[i] << endl;
	
    // Substract two matrices
        subsA0 = a[i].v2.x-a[i].v1.x;
        subsA1 = a[i].v2.y-a[i].v1.y;
        subsA2 = a[i].v2.z-a[i].v1.z;

        subsB0 = a[i].v3.x-a[i].v2.x;
        subsB1 = a[i].v3.y-a[i].v2.y;
        subsB2 = a[i].v3.z-a[i].v2.z;

        //cout << setw(10) << "A: ";
        //cout << setw(13) << subsA0;
        //cout << setw(13) << subsA1;
        //cout << setw(13) << subsA2 << endl;

        //cout << setw(10) << "B: ";
        //cout << setw(13) << subsB0;
        //cout << setw(13) << subsB1;
        //cout << setw(13) << subsB2 << endl;
	
    // Calculate cross product 
        crossx[i] = subsA1*subsB2 - subsB1*subsA2;
        crossy[i] = subsB0*subsA2 - subsA0*subsB2;
        crossz[i] = subsA0*subsB1 - subsB0*subsA1;

        //cout << setw(10) << "Cross: ";
        //cout << setw(13) << crossx[i];
        //cout << setw(13) << crossy[i];
        //cout << setw(13) << crossz[i] << endl;
    }
    stopwatch = tmr.elapsed();
    cout << "Triangle calculations: " << stopwatch << endl;
	
    // Calculate Bond Direction 
    for (i=0; i<sizeB; i++)
    { 
        dir[i].x = b[i].v2.x-b[i].v1.x;
        dir[i].y = b[i].v2.y-b[i].v1.y;
        dir[i].z = b[i].v2.z-b[i].v1.z;

        //cout << setw(10) << "Bond Dir: ";
        //cout << setw(13) << dir[i].x;
        //cout << setw(13) << dir[i].y;
        //cout << setw(13) << dir[i].z << endl;
    }
	
    ofstream file;
    file.open("Intersection_pts.pdb");
    int k = 0;
    int m = 0;

    double tX, tY, tZ;
    double intersectX, intersectY, intersectZ;

    tmr.reset();

    int intSize = sizeT*sizeB;

    Vertex * vertices;
    vertices = (Vertex*)malloc(intSize*sizeof(Vertex));

    Triangle* d_a;
    Bond* d_b;
    cudaMalloc(&d_a, sizeT*sizeof(Triangle));
    cudaMalloc(&d_b, sizeB*sizeof(Bond));

    cudaFree(d_a);
    cudaFree(d_b);

    for (i=0; i<sizeT; i++)
    {
	m++;
        for (j=0;j<sizeB;j++)
        {
            vertices[sizeT+sizeT*sizeB] = calcIntersect(&a[i], &b[j]);
            cout << vertices[sizeT+sizeT*sizeB].x << endl;

            stopwatch = tmr.elapsed();
            cout << "First intersection calculated: " << stopwatch << endl;

            //x[i] = b[i][3]+t[i]*dir[i][j];
            //cout << "Value of t = " << " " << (tx[i]+ty[i]+tz[i]) << "\n" ;
            //file << "Intersection points are  = " << " " << t1[i] << " "
            //    << t2[i] << " " << t3[i] << "\n" ;
            /*
            file << "ATOM  ";           // "ATOM"
            file << setw(5) << k << " ";// Atom number
            file << "CA  ";             // Atom name
            file << " ";                // Alternate location indicator
	    file << "HIS ";             // Residue name
            file << "A";                // Chain
            file << setw(4) << m;       // Residue number
            file << " " << "   ";       // Code for insertions of residues
            file << setw(8) << fixed << setprecision(5) << intersectX;   // X position
            file << setw(8) << fixed << setprecision(5) << intersectY;   // Y position
            file << setw(8) << fixed << setprecision(5) << intersectZ;   // Z position
	    file << "    0";            // Occupancy
	    file << "                    "; // Everything else
	    file << endl;
            */
            
            if (k==1) // Timer
            {
                stopwatch = tmr.elapsed();
                cout << "Intersection calcs and writing: " << stopwatch << endl;
            }
        }
    }
    stopwatch = tmr.elapsed();
    cout << "All intersection calculation time: " << stopwatch << endl;
    file.close();

    return 0;
}
