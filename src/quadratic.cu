#include <chrono>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;
#if 1
#define CUERR { cudaError_t err; \
  if ((err = cudaGetLastError()) != cudaSuccess) { \
  printf("CUDA error: %s, %s line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
  printf("Thread aborting...\n"); \
  return NULL; }}
#else
#define CUERR
#endif


class Timer
{
public:
    Timer() : beg_(clock_::now()) {}
    void reset()
    {
        beg_ = clock_::now();
    }
    double elapsed() const
    { 
        return std::chrono::duration_cast<second_>
            (clock_::now() - beg_).count();
    }

private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
};
struct Vertex {
    float x, y, z;
};
struct Bond {
    Vertex v1, v2;
};
struct Triangle {
    Vertex v1, v2, v3;
};

//
// global atomic counter
//
__device__ unsigned int isectatomic[3] = {0, 0, 0};

__device__ void reset_atomic_counter(unsigned int *counter) {
 counter[0] = 0;
 __threadfence();
}

inline __device__
Vertex calcIntersect(Triangle * t, Bond * b)
{
    float inv3 = 1/3.0;

    float centX = (t->v1.x+t->v2.x+t->v3.x)*inv3;
    float centY = (t->v1.y+t->v2.y+t->v3.y)*inv3;
    float centZ = (t->v1.z+t->v2.z+t->v3.z)*inv3;

    // Substract two matrices
    float subsA0 = t->v2.x-t->v1.x;
    float subsA1 = t->v2.y-t->v1.y;
    float subsA2 = t->v2.z-t->v1.z;

    float subsB0 = t->v3.x-t->v2.x;
    float subsB1 = t->v3.y-t->v2.y;
    float subsB2 = t->v3.z-t->v2.z;

    // Calculate cross product 
    float crossX = subsA1*subsB2 - subsB1*subsA2;
    float crossY = subsB0*subsA2 - subsA0*subsB2;
    float crossZ = subsA0*subsB1 - subsB0*subsA1;

    double tX = (centX - b->v1.x)*crossX;
    double tY = (centY - b->v1.y)*crossY;
    double tZ = (centZ - b->v1.z)*crossZ;

    // Calculate bond direction
    float dirX = b->v2.x-b->v1.x;
    float dirY = b->v2.y-b->v1.y;
    float dirZ = b->v2.z-b->v1.z;

    float tSum = tX+tY+tZ;

    float tFactor = tSum/( dirX*crossX + dirY*crossY + dirZ*crossZ );

    Vertex intersect;
    if (tSum>=0 && tSum<=1)
    {
        intersect.x
            = dirX * tFactor + b->v1.x;

        intersect.y
            = dirY * tFactor + b->v1.y;

        intersect.z
            = dirZ * tFactor + b->v1.z;
        return intersect;
    }
    intersect.x = 0, intersect.y =0, intersect.z=0;
    return intersect;
}

Vertex calcIntersectCPU(Triangle * t, Bond * b)
{
    float inv3 = 1/3.0;

    float centX = (t->v1.x+t->v2.x+t->v3.x)*inv3;
    float centY = (t->v1.y+t->v2.y+t->v3.y)*inv3;
    float centZ = (t->v1.z+t->v2.z+t->v3.z)*inv3;

    // Substract two matrices
    float subsA0 = t->v2.x-t->v1.x;
    float subsA1 = t->v2.y-t->v1.y;
    float subsA2 = t->v2.z-t->v1.z;

    float subsB0 = t->v3.x-t->v2.x;
    float subsB1 = t->v3.y-t->v2.y;
    float subsB2 = t->v3.z-t->v2.z;

    // Calculate cross product 
    float crossX = subsA1*subsB2 - subsB1*subsA2;
    float crossY = subsB0*subsA2 - subsA0*subsB2;
    float crossZ = subsA0*subsB1 - subsB0*subsA1;

    double tX = (centX - b->v1.x)*crossX;
    double tY = (centY - b->v1.y)*crossY;
    double tZ = (centZ - b->v1.z)*crossZ;

    // Calculate bond direction
    float dirX = b->v2.x-b->v1.x;
    float dirY = b->v2.y-b->v1.y;
    float dirZ = b->v2.z-b->v1.z;

    float tSum = tX+tY+tZ;

    float tFactor = tSum/( dirX*crossX + dirY*crossY + dirZ*crossZ );

    Vertex intersect;
    if (tSum>=0 && tSum<=1)
    {
        intersect.x
            = dirX * tFactor + b->v1.x;

        intersect.y
            = dirY * tFactor + b->v1.y;

        intersect.z
            = dirZ * tFactor + b->v1.z;
        return intersect;
    }
    intersect.x = 0, intersect.y =0, intersect.z=0;
    return intersect;
}


__global__
void calcCaller(Triangle * t, Bond * b, Vertex * intersects,
                int sizeT, int sizeB)
{
    int bx = blockIdx.x * blockDim.x + threadIdx.x;
    int ty = blockIdx.y * blockDim.y + threadIdx.y;
    int i_i = bx + ty * sizeB;

    if (bx < sizeB && ty < sizeT)
    {
        Vertex isect = calcIntersect(&t[ty], &b[bx]);
        if (isect.x) {
#if 1  || __CUDA_ARCH__ >= 200
          __threadfence();
          unsigned int isectidx = atomicInc(&isectatomic[0], 1);
          intersects[isectidx] = isect;
#else
#error this code requires hardware atomic increment support...
#endif
        }
    }
}

__global__ void clear_isect_atomic() {
#if 1 || __CUDA_ARCH__ >= 200
  reset_atomic_counter(&isectatomic[0]);
#endif
}


int main ()
{
    Timer tmr;
    double stopwatch = tmr.elapsed();
    cout << stopwatch << endl;


    stopwatch = tmr.elapsed();
    cout << stopwatch << endl;

    string userInput;
    cout << "Input triangle file name (testInputs/dragon_vrip.dat): ";
    getline(cin,userInput);

    ifstream inputFileT;
    inputFileT.open(userInput);

    int i=0,j=0;

    // Assign triangle vertex coordinates to matrix 
    //
    // Array containing vertices of each triangle face
    //

    string headerT;
    unsigned sizeT;

    inputFileT >> headerT;
    inputFileT >> sizeT;
    
    // Data containing values of cross product

    Triangle* t = NULL;
    t = new Triangle[sizeT];
    //cout << headerT << " " << sizeT;

    tmr.reset();

    for (i=0; i<sizeT; i++)
    {
        inputFileT >> t[i].v1.x;
        inputFileT >> t[i].v1.y;
        inputFileT >> t[i].v1.z;
        inputFileT >> t[i].v2.x;
        inputFileT >> t[i].v2.y;
        inputFileT >> t[i].v2.z;
        inputFileT >> t[i].v3.x;
        inputFileT >> t[i].v3.y;
        inputFileT >> t[i].v3.z;
        //cout<<"\n";
    }
    stopwatch = tmr.elapsed();
    cout << "Triangle read time: " << stopwatch << endl;
    inputFileT.close();
    
    cout << "Input bond file name (testInputs/full_bonds.dat): ";
    getline(cin,userInput);

    ifstream inputFileBond;
    inputFileBond.open(userInput);
    string headerB;
    unsigned sizeB;

    inputFileBond >> headerB;
    inputFileBond >> sizeB;
    //cout << headerB << " " << sizeB;

    // Array containing points in each line segment
    Bond* b = NULL;
    b = new Bond[sizeB];

    // Read Bonds
    i = 0;
    j = 0;
//    cout << "I am outside triangle" << "\n";
    for (i=0; i<sizeB; i++)
    {
        inputFileBond >> b[i].v1.x;
        inputFileBond >> b[i].v1.y;
        inputFileBond >> b[i].v1.z;
        inputFileBond >> b[i].v2.x;
        inputFileBond >> b[i].v2.y;
        inputFileBond >> b[i].v2.z;
    }

    inputFileBond.close();

    tmr.reset();
	
    ofstream file;
    file.open("Intersection_pts.txt");
    int k = 0;
    int m = 0;

    tmr.reset();

    int intSize = sizeT*sizeB;

    int sizeOfVertices = intSize*sizeof(Vertex);

    Vertex * intersects;
    intersects = (Vertex*)malloc(sizeOfVertices);

    Vertex * d_intersects;
    cudaMalloc(&d_intersects, sizeOfVertices);

    Triangle* d_t;
    Bond* d_b;

    int sizeOfTriangles = sizeT*sizeof(Triangle);
    int sizeOfBonds     = sizeB*sizeof(Bond);


    cudaMalloc(&d_t, sizeOfTriangles);
    cudaMalloc(&d_b, sizeOfBonds);
    cudaMemcpy (d_t, t, sizeOfTriangles, cudaMemcpyHostToDevice);
    cudaMemcpy (d_b, b, sizeOfBonds, cudaMemcpyHostToDevice);

    //cudaDeviceSynchronize();
    stopwatch = tmr.elapsed();
    cout << "Data migration time: " << stopwatch << endl; 

    tmr.reset();

    dim3 Bsz = dim3(64, 1, 1);
    dim3 dimGrid((sizeB+(Bsz.x-1))/Bsz.x, (sizeT+(Bsz.y-1)/Bsz.y));
    clear_isect_atomic<<<1, 1>>>();
    calcCaller<<<dimGrid, Bsz>>>(d_t, d_b, d_intersects, sizeT, sizeB);

    cout << "About to run ..." << endl; 
    cudaDeviceSynchronize();
    tmr.reset();
    clear_isect_atomic<<<1, 1>>>();
    calcCaller<<<dimGrid, Bsz>>>(d_t, d_b, d_intersects, sizeT, sizeB);
    cudaDeviceSynchronize();
    stopwatch = tmr.elapsed();
    cout << "All intersection calculation time: " << stopwatch << endl; 

    tmr.reset();
    unsigned int numisects = 0;
    cudaMemcpy(&numisects, &isectatomic[0], sizeof(unsigned int), cudaMemcpyDeviceToHost);
    printf("Number of intersections: %u\n", numisects);

    cudaMemcpy (intersects, d_intersects, sizeOfVertices, cudaMemcpyDeviceToHost);
    stopwatch = tmr.elapsed();
    cout << "Data retrieve time: " << stopwatch << endl; 

    cudaFree(d_t);
    cudaFree(d_b);
    cudaFree(d_intersects);
    stopwatch = tmr.elapsed();
    Vertex cpuVertex;

    tmr.reset();
    for (i=0; i<sizeT; i++)
    {
	//m++;
        for (j=0;j<sizeB;j++)
        {
            cpuVertex = calcIntersectCPU(&t[i], &b[j]);
            //cout << "cpuVertex: " << cpuVertex.x << " " << cpuVertex.y << " " << cpuVertex.z << endl;
            //cout << "gpuVertex: " << intersects[j+sizeT*i].x;
            //cout << "gpuVertex: " << intersects[j+sizeB*i].x;
            //cout << " " << intersects[j+sizeT*i].y << " " << intersects[j+sizeT*i].z << endl;
            //cout << " " << intersects[j+sizeB*i].y << " " << intersects[j+sizeB*i].z << endl;

            //stopwatch = tmr.elapsed();
            //cout << "First intersection calculated: " << stopwatch << endl;

            //x[i] = b[i][3]+t[i]*dir[i][j];
            //cout << "Value of t = " << " " << (tx[i]+ty[i]+tz[i]) << "\n" ;
            //file << "Intersection points are  = " << " " << t1[i] << " "
            //    << t2[i] << " " << t3[i] << "\n" ;
            /*
            file << "ATOM  ";           // "ATOM"
            file << setw(5) << k << " ";// Atom number
            file << "CA  ";             // Atom name
            file << " ";                // Alternate location indicator
	    file << "HIS ";             // Residue name
            file << "A";                // Chain
            file << setw(4) << m;       // Residue number
            file << " " << "   ";       // Code for insertions of residues
            file << setw(8) << fixed << setprecision(5) << intersectX;   // X position
            file << setw(8) << fixed << setprecision(5) << intersectY;   // Y position
            file << setw(8) << fixed << setprecision(5) << intersectZ;   // Z position
	    file << "    0";            // Occupancy
	    file << "                    "; // Everything else
	    file << endl;
            */
            /*
            if (k==1) // Timer
            {
                stopwatch = tmr.elapsed();
                cout << "Intersection calcs and writing: " << stopwatch << endl;
            }*/
        }
    }
    stopwatch = tmr.elapsed();
    cout << "CPU calculation time: " << stopwatch << endl;
    file.close();
    return 0;
}
